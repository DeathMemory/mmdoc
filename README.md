##梆梆的请求在取完订单号后发送

## 获取梆梆初始化请求
如果是初始化操作只需要，请求此url

url: /bb/getClearResult

| 键值        | 参考  | 说明    |
| --------   | ----- | ----  |
| riskStubId   | ----- | 第一次请求传 0，之后的请求按返回的值传  |
| riskStubTime   | ----- | 第一次请求传 0，之后的请求按返回的值传  |

返回

| 键值        | 参考  | 说明    |
| --------   | ----- | ----  |
| url   | ----- | 向官方提交的 url  |
| content   | ----- | base64 编码后的内容，解密后以 byte[] 的形式发给官方  |


## 获取梆梆请求拼包组合
url: /bb/getRequests

| 键值        | 参考  | 说明    |
| --------   | ----- | ----  |
|serial| ---- |Build.SERIAL|
|board| ---- |Build.BOARD|
|bootloader| ---- |Build.BOOTLOADER|
|device| ---- |Build.DEVICE|
|display| ---- |Build.DISPLAY|
|hardware| ---- |Build.HARDWARE|
|build_id| ---- |Build.ID|
|fingerprint| ---- |Build.FINGERPRINT|
|cpu_abi| ---- |Build.CPU_ABI|
|cpu_abi2| ---- |Build.CPU_ABI2|
|android_id| ---- |android_id|
|manufacturer| "LGE" | ---- |
|imsi| ---- | ---- |
|os_version| "4.4.4" | ---- |
|brand| ---- | ---- |
|host| "kpfj3.cbf.corp.google.com" | Build.HOST |
|mac| xx : xx : xx : xx : xx : xx | ---- |
|bht_mac| 蓝牙地址 |bluetooth_address|
|udid| "de158fe1-3bc9-36fa-84a2-e6465c8fc4ef" | 获取详见 getUDID 方法 |
| uid | 10079 | 获取详见 findUID 方法 |
| agentId | 115 | ---- |
| tradeId | 0875052E6 | 获取短信接口返回的 tradeid |
|clearcontent| ---- | 初始化 getClearResult 请求的返回内容，以 base64 加密的形式传输过来 |
|packageName| "com.ourgame.mahjong.bmhber" | 自己的包名 |
|appName| "火爆二人麻将" | 自己的应用名 |
|manifestMd5| "35e227fd87c1d4ff4a0389258403f9a0" | 自己包 manifest 的 md5 值 |
|cert_issure| "CN=CMCA application signing CA, C=CN" | ---- |
|cert_md5| "e1055ddf4c25e3dfb4a7a3607555b942" | ---- |
|ver_name| "5.3.0" | 版本号字符串 |
|ver_code| 33 | 版本代码 |
|installTime| 1484564000259L | 安装时间 |
|updateTime| 1484564000259L | 更新时间 |


返回是一个 json 数组，需要按顺序将数组中 content 对应 url 逐个向官方发送

| 键值        | 参考  | 说明    |
| --------   | ----- | ----  |
| url   | ----- | 向官方提交的 url  |
| content   | ----- | base64 编码后的内容，解密后以 byte[] 的形式发给官方  |
| riskStubId | ----- | 需要本地保存，下次提交 getClearResult 时需要使用 |
| riskStubTime | ----- | 需要本地保存，下次提交 getClearResult 时需要使用  |

```
#!java

    private static String getUDID(Context context){
		
		String result = "";
		String android_id = Settings.Secure.getString(context.getContentResolver(), "android_id");
		
		
		UUID udid = null;
		try {
			udid = UUID.nameUUIDFromBytes(android_id.getBytes("utf8"));
		} catch (UnsupportedEncodingException e) {
		}
		if(udid != null){
			result = udid.toString();
		}
		return result;
		
		
	}

```

```
#!java
	private static int findUID(Context context) {
        int i;
        try {
            i = context.getPackageManager().getApplicationInfo(context.getPackageName(), 1).uid;
        }
        catch(Exception e) {
            e.printStackTrace();
            i = 0xFFFFFFFF;
        }

        return i;
    }
```